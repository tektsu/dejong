package com.xecko.attr;

import java.awt.Color;
import java.util.ArrayList;
import java.util.ListIterator;

public class Colorize {

    ArrayList<Element> elements;

    private class Element {

        int minRed;
        int minGreen;
        int minBlue;
        int maxRed;
        int maxGreen;
        int maxBlue;
        int start;
        int threshold;
        double pctRange;

        Element(String colorString, Element lastElement) {
            String[] components = colorString.split(":");
            pctRange = components.length > 1 ? Math.abs(Integer.parseInt(components[1])) : 0;
            String[] colors = components[0].split("-");
            Color color0 = Color.decode(colors[0]);
            maxRed = color0.getRed();
            maxGreen = color0.getGreen();
            maxBlue = color0.getBlue();
            if (colors.length > 1) {
                Color color1 = Color.decode(colors[1]);
                minRed = maxRed;
                maxRed = color1.getRed();
                minGreen = maxGreen;
                maxGreen = color1.getGreen();
                minBlue = maxBlue;
                maxBlue = color1.getBlue();
            } else {
                minRed = 0;
                minGreen = 0;
                minBlue = 0;
                if (lastElement != null) {
                    minRed = lastElement.maxRed;
                    minGreen = lastElement.maxGreen;
                    minBlue = lastElement.maxBlue;
                }
            }
            start = 0;
            threshold = 0;
        }

    }

    public Colorize(String[] colors) {
        elements = new ArrayList<Element>();
        Element lastElement = null;
        for (String colorString : colors) {
            Element e = new Element(colorString, lastElement);
            elements.add(e);
            lastElement = e;
        }
    }

    public void setLinearThresholds(int max) {
        int pctStandard = (int) (100.0 / elements.size());
        int pctTotalRequested = 0;
        int nextStart = 0;
        int count = 0;
        for (final ListIterator<Element> i = elements.listIterator(); i.hasNext();) {
            final Element element = i.next();
            element.pctRange = (element.pctRange == 0 ? pctStandard : element.pctRange);
            pctTotalRequested += element.pctRange;
            i.set(element);
        }
        for (final ListIterator<Element> i = elements.listIterator(); i.hasNext();) {
            final Element element = i.next();
            element.start = nextStart;

            element.pctRange = (100.0
                    * ((element.pctRange == 0 ? pctStandard : element.pctRange) / (double) pctTotalRequested));
            element.threshold = (int) (count == elements.size() - 1 ? max : nextStart + max * element.pctRange / 100.0);
            i.set(element);
            nextStart = element.threshold + 1;
            count++;
        }
    }

    public void setProportionalThresholds(Image image) {
        double[] hist = image.getHistogramPercentages();
        int pctStandard = (int) (100.0 / elements.size());
        int pctTotalRequested = 0;
        int nextStart = 0;
        int count = 0;
        for (final ListIterator<Element> i = elements.listIterator(); i.hasNext();) {
            final Element element = i.next();
            element.pctRange = (element.pctRange == 0 ? pctStandard : element.pctRange);
            pctTotalRequested += element.pctRange;
            i.set(element);
        }

        double totalPercent = 100.0 - hist[0]; // Exclude background from calculations
        for (final ListIterator<Element> i = elements.listIterator(); i.hasNext();) {
            final Element element = i.next();

            element.start = nextStart;
            element.pctRange = (100.0
                    * ((element.pctRange == 0 ? pctStandard : element.pctRange) / (double) pctTotalRequested));
            double percentageNeeded = element.pctRange * totalPercent / 100;
            System.out.printf("Percent needed: %f\n", percentageNeeded);
            double percentageFound = 0.0;
            int cursor;
            for (cursor = element.start; cursor < hist.length; cursor++) {
                if (cursor > 0) {
                    percentageFound += hist[cursor];
                }
                if (percentageFound > percentageNeeded) {
                    break;
                }
            }
            element.threshold = (int) (count == elements.size() - 1 ? image.getMaxFrequency() : cursor);
            i.set(element);
            nextStart = element.threshold + 1;
            count++;
        }
    }

    public int getColor(int level) {
        Element e = null;

        for (final ListIterator<Element> i = elements.listIterator(); i.hasNext();) {
            e = i.next();
            if (level <= e.threshold) {
                break;
            }
        }

        double pct = ((double) level - e.start) / (e.threshold - e.start);
        return new Color((int) (pct * (e.maxRed - e.minRed) + e.minRed),
                (int) (pct * (e.maxGreen - e.minGreen) + e.minGreen), (int) (pct * (e.maxBlue - e.minBlue) + e.minBlue))
                        .getRGB();
    }

    public int getColor(int level, int maxLevel, int velocity) {

        // double pctLevel = (double) level / maxLevel;

        Element e = null;

        for (final ListIterator<Element> i = elements.listIterator(); i.hasNext();) {
            e = i.next();
            if (velocity <= e.threshold) {
                break;
            }
        }

        double pct = ((double) velocity - e.start) / (e.threshold - e.start);
        int red = (int) (pct * (e.maxRed - e.minRed) + e.minRed);
        int green = (int) (pct * (e.maxGreen - e.minGreen) + e.minGreen);
        int blue = (int) (pct * (e.maxBlue - e.minBlue) + e.minBlue);
        // if (pctLevel < 0.5) {
        // red = (int) (red - red * (1.0 - pctLevel));
        // green = (int) (green - green * (1.0 - pctLevel));
        // blue = (int) (blue - blue * (1.0 - pctLevel));
        // } else {
        // red = (int) (red + (255 - red) * (pctLevel - 0.5));
        // green = (int) (green + (255 - green) * (pctLevel - 0.5));
        // blue = (int) (blue + (255 - blue) * (pctLevel - 0.5));
        // }
        // System.out.printf("%d %d %d %f\n", red, green, blue, pctLevel);
        return new Color(red, green, blue).getRGB();
    }

    public void showColors() {
        for (final ListIterator<Element> i = elements.listIterator(); i.hasNext();) {
            final Element element = i.next();
            System.out.printf("(%3d %3d %3d) - (%3d %3d %3d) [%5d - %5d] size: %5d p: %5.2f%%\n", element.minRed,
                    element.minGreen, element.minBlue, element.maxRed, element.maxGreen, element.maxBlue, element.start,
                    element.threshold, element.threshold - element.start, element.pctRange);
        }
    }

}
