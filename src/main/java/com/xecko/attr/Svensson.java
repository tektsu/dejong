package com.xecko.attr;

public class Svensson extends IFS {

    private double a;
    private double b;
    private double c;
    private double d;

    Svensson(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;

        // Random starting point
        x = (Math.random() * 4) - 2;
        y = (Math.random() * 4) - 2;
    }

    public double[] getCurrent() {
        return new double[] { x, y };
    }

    public double[] getNext() {
        double[] point = new double[2];

        point[0] = d * Math.sin(a * x) - Math.sin(b * y);
        point[1] = c * Math.cos(a * x) + Math.cos(b * y);
        x = point[0];
        y = point[1];

        return point;
    }

    public double[] getConstants() {
        return new double[] { a, b, c, d };
    }

    // Get absoute value of max range
    public double getMaxRange() {
        return Math.max(Math.abs(d), Math.abs(c)) + 1;
    }

    public void summarize() {
        System.out.printf("Svensson Constants:\n");
        System.out.printf("  a = %8.5f\n", a);
        System.out.printf("  b = %8.5f\n", b);
        System.out.printf("  c = %8.5f\n", c);
        System.out.printf("  d = %8.5f\n", d);
    }
}
