package com.xecko.attr;

import java.io.Serializable;

public class Image implements Serializable {

    private static final long serialVersionUID = 1L;

    private long totalPoints; // Number of points generated
    private long uniquePoints; // Number of points changed
    private int maxFrequency; // Highest value of any point
    private double maxVelocity; // Highest velocity of any point
    private int size; // Image dimensions (image is square)
    private int[][] frequencies; // Point levels
    private double[][] velocities; // Total point velocities

    public Image(int size) {
        this.size = size;
        frequencies = new int[this.size][this.size];
        velocities = new double[this.size][this.size];

        totalPoints = 0;
        uniquePoints = 0;
        maxFrequency = 0;
        maxVelocity = 0;
    }

    public int addPoint(int[] point, double velocity) {

        totalPoints++;

        if (point[0] < 0 || point[0] >= size || point[1] < 0 || point[1] >= size) { // Ignore out-of-bounds pixels
            return 0;
        }

        if (frequencies[point[0]][point[1]] == 0) {
            uniquePoints++;
        }
        int pVal = ++frequencies[point[0]][point[1]];
        if (pVal > maxFrequency) {
            maxFrequency = pVal;
        }

        velocities[point[0]][point[1]] += velocity;
        if (velocity > maxVelocity)
            maxVelocity = velocity;

        return pVal;
    }

    public int addPoint(double[] point, double velocity) {
        int[] p = new int[2];
        p[0] = (int) point[0];
        p[1] = (int) point[1];
        return addPoint(p, velocity);
    }

    public int[] getHistogram() {
        int[] h = new int[maxFrequency + 1];

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                h[frequencies[x][y]]++;
            }
        }

        return h;
    }

    public double[] getHistogramPercentages() {
        double[] hist = new double[maxFrequency + 1];
        int[] n = getHistogram();
        for (int i = 0; i < n.length; i++) {
            hist[i] = 100 * n[i] / (double) (size * size);
        }

        return hist;
    }

    public int getSize() {
        return size;
    }

    public int getFrequency(int x, int y) {
        return frequencies[x][y];
    }

    public int getVelocity(int x, int y) {
        return (int) (velocities[x][y] / frequencies[x][y]);
    }

    public long getTotalPoints() {
        return totalPoints;
    }

    public long getUniquePoints() {
        return uniquePoints;
    }

    public int getMaxFrequency() {
        return maxFrequency;
    }

    public int getMaxVelocity() {
        return (int) maxVelocity;
    }
}
