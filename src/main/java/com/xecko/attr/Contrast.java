package com.xecko.attr;

// Apply a log curve to increate contrast.
public class Contrast {

    double multiplier;

    public Contrast(int maxValue) {
        multiplier = maxValue / Math.log10(maxValue);
    }

    int enhance(int value) {
        return (int) (multiplier * Math.log10(value + 1));
    }

}
