package com.xecko.attr;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.Callable;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * dejong generate
 *
 */
@Command(name = "generate", mixinStandardHelpOptions = true, version = "attr generate 1.0", description = "Generate attractor images.")
public class Generate implements Callable<Integer> {

    //
    // Define command line options
    //

    enum AlgorithmType {
        dejong, svensson
    }

    enum ImageType {
        gray16, color24
    }

    @Option(names = { "-g",
            "--algorithm" }, defaultValue = "dejong", description = "The algorithm used to generate the points. Possible values: ${COMPLETION-CANDIDATES}. Default: ${DEFAULT-VALUE}.")
    private AlgorithmType algorithmType;

    @Option(names = { "-s",
            "--size" }, defaultValue = "800", description = "Size in pixels, used for both width and height of image. Default: ${DEFAULT-VALUE}.")
    private int size;

    @Option(names = { "-t",
            "--threshold" }, defaultValue = "65535", description = "Stop processing when any pixel has been generated this many times. Default: ${DEFAULT-VALUE}.")
    private int threshold;

    @Option(names = "-a", defaultValue = "-2.0", description = "The 'a' constant. How it is used depends on the algorithm chosen. Default: ${DEFAULT-VALUE}.")
    private double a;

    @Option(names = "-b", defaultValue = "-2.0", description = "The 'b' constant. How it is used depends on the algorithm chosen. Default: ${DEFAULT-VALUE}.")
    private double b;

    @Option(names = "-c", defaultValue = "-2.0", description = "The 'c' constant. How it is used depends on the algorithm chosen. Default: ${DEFAULT-VALUE}.")
    private double c;

    @Option(names = "-d", defaultValue = "-2.0", description = "The 'd' constant. How it is used depends on the algorithm chosen. Default: ${DEFAULT-VALUE}.")
    private double d;

    @Option(names = { "-r",
            "--randomize-constants" }, defaultValue = "false", description = "Randomize all four constants. Default: ${DEFAULT-VALUE}.")
    private boolean randomizeConstants;

    @Option(names = { "--ra",
            "--randomize-a" }, defaultValue = "false", description = "Randomize the 'a' constant. Default: ${DEFAULT-VALUE}.")
    private boolean randomizeA;

    @Option(names = { "--rb",
            "--randomize-b" }, defaultValue = "false", description = "Randomize the 'b' constant. Default: ${DEFAULT-VALUE}.")
    private boolean randomizeB;

    @Option(names = { "--rc",
            "--randomize-c" }, defaultValue = "false", description = "Randomize the 'c' constant. Default: ${DEFAULT-VALUE}.")
    private boolean randomizeC;

    @Option(names = { "--rd",
            "--randomize-d" }, defaultValue = "false", description = "Randomize the 'd' constant. Default: ${DEFAULT-VALUE}.")
    private boolean randomizeD;

    @Option(names = { "-f",
            "--scale-factor" }, defaultValue = "90", description = "The percentage of the image used for the image. If set to 100, the image will have no border. Default: ${DEFAULT-VALUE}.")
    private double scaleFactor;

    @Option(names = { "-v", "--verbose" }, description = "If set, print a summary of the work accomplished.")
    boolean verbose;

    String outputFileFormat = "png";

    @Option(names = { "--od",
            "--output-dir" }, defaultValue = ".", description = "The directory in which to place the output. Default: '${DEFAULT-VALUE}'.")
    String outputDirectory;

    @Option(names = { "-o", "--output-file" }, description = "The base name of the output file, without an extension.")
    String outputFile = "test";

    @Option(names = "--color", description = "Color ranges. Example: #12439F-#FF456E:10. If the first color is omitted, the end of the most recently specified range is used. The number after the colon is optional and is the percentage if the full range to use these colors. Percentages will be adjusted if they don't add up to 100%%. --color may appear multiple times, and color ranges are used in order the appear on the command line.")
    String[] colors;

    @Option(names = { "--cs",
            "--color-scheme" }, defaultValue = "frequency", description = "The scheme used to color the image. Only useful if --color is also used. Possible values: ${COMPLETION-CANDIDATES}. Default: '${DEFAULT-VALUE}'.")
    ColorScheme colorScheme;

    public static void main(String... args) {
        int exitCode = new CommandLine(new Attractor()).execute(args);
        System.exit(exitCode);
    }

    //
    // Impement generate command
    //
    @Override
    public Integer call() {

        // Randomize constants
        if (randomizeA || randomizeConstants) {
            a = (Math.random() * 2 * Math.PI) - Math.PI;
        }
        if (randomizeB || randomizeConstants) {
            b = (Math.random() * 2 * Math.PI) - Math.PI;
        }
        if (randomizeC || randomizeConstants) {
            c = (Math.random() * 2 * Math.PI) - Math.PI;
        }
        if (randomizeD || randomizeConstants) {
            d = (Math.random() * 2 * Math.PI) - Math.PI;
        }

        // Initialize the output system
        Output output;
        try {
            output = new Output(outputDirectory, outputFile, outputFileFormat);
        } catch (FileNotFoundException e) {
            System.out.println("\nERROR: " + e);
            return 1;
        }

        // Process colors
        Colorize colorize = null;
        if (colors != null) {
            try {
                colorize = new Colorize(colors);
            } catch (NumberFormatException e) {
                System.out.println("\nERROR: " + e);
                return 1;
            }
        }

        if (verbose) {
            System.out.printf("Generating a %dx%d %s image in %s.*\n", size, size, algorithmType,
                    output.getBaseFilename());
        }

        IFS algorithm = null;
        if (algorithmType == AlgorithmType.dejong)
            algorithm = new DeJong(a, b, c, d);
        if (algorithmType == AlgorithmType.svensson)
            algorithm = new Svensson(a, b, c, d);
        Canvas canvas = new Canvas(size, scaleFactor, algorithm, threshold);

        if (verbose) {
            canvas.summarize();
        }

        // Output results

        ImageType type = colors == null ? ImageType.gray16 : ImageType.color24;

        try {
            if (type == ImageType.gray16)
                output.writeGray16(canvas.getImage());
            if (type == ImageType.color24)
                output.writeColor24(canvas.getImage(), colorize, colorScheme);
        } catch (IOException e) {
            System.out.println("\nERROR: " + e);
            return 1;
        }

        return 0;
    }
}
