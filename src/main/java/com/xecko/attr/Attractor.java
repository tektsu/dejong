package com.xecko.attr;

import java.util.concurrent.Callable;

import picocli.CommandLine;
import picocli.CommandLine.Command;

/**
 * attr
 *
 */
@Command(name = "attractor", subcommands = Generate.class, mixinStandardHelpOptions = true, version = "attr 1.0", description = "Work with attractor images.")
public class Attractor implements Callable<Integer> {

    public static void main(String... args) {
        int exitCode = new CommandLine(new Attractor()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Integer call() {
        CommandLine.usage(new Attractor(), System.out);
        return 0;
    }
}
