package com.xecko.attr;

public class IFS {

    protected double x;
    protected double y;

    IFS() {
        // Random starting point
        x = Math.random();
        y = Math.random();
    }

    public double[] getCurrent() {
        return new double[] { x, y };
    }

    public double[] getNext() {

        x += Math.random();
        y += Math.random();

        return new double[] { x, y };
    }

    public double[] getConstants() {
        return new double[] { 0 };
    }

    // Get absoute value of max range
    public double getMaxRange() {
        return 10.0;
    }

    public void summarize() {
    }
}
