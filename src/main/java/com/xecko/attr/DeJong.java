package com.xecko.attr;

public class DeJong extends IFS {

    private double a;
    private double b;
    private double c;
    private double d;

    DeJong(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;

        // Random starting point
        x = (Math.random() * 4) - 2;
        y = (Math.random() * 4) - 2;
    }

    public double[] getCurrent() {
        return new double[] { x, y };
    }

    public double[] getNext() {
        double[] point = new double[2];

        point[0] = Math.sin(a * y) - Math.cos(b * x);
        point[1] = Math.sin(c * x) - Math.cos(d * y);
        x = point[0];
        y = point[1];

        return point;
    }

    public double[] getConstants() {
        return new double[] { a, b, c, d };
    }

    // Get absoute value of max range
    public double getMaxRange() {
        return 2.0; // de Jong always gives coordinate values between -2 and 2
    }

    public void summarize() {
        System.out.printf("de Jong Constants:\n");
        System.out.printf("  a = %8.5f\n", a);
        System.out.printf("  b = %8.5f\n", b);
        System.out.printf("  c = %8.5f\n", c);
        System.out.printf("  d = %8.5f\n", d);
    }
}
