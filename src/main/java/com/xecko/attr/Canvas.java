package com.xecko.attr;

import java.awt.geom.Point2D;

public class Canvas {

    private int size;
    private double scaleFactor;
    private IFS algorithm;
    private int threshold;
    private Image image;

    public Canvas(int size, double scaleFactor, IFS algorithm, int threshold) {

        this.size = size;
        this.scaleFactor = scaleFactor;
        this.algorithm = algorithm;
        this.threshold = threshold;

        generateImage();
    }

    private void generateImage() {

        // Create a scaler appropriate to the algorithm
        Scaler scaler = new Scaler(((scaleFactor / 100) * size / 2) / algorithm.getMaxRange(), size / 2);

        // Generate the image data
        this.image = new Image(size);
        for (long i = 0; i < 10000000000L; i++) {

            double[] lastPoint = scaler.scalePoint(algorithm.getCurrent());
            double[] nextPoint = scaler.scalePoint(algorithm.getNext());

            int max = image.addPoint(nextPoint,
                    Point2D.distance(lastPoint[0], lastPoint[1], nextPoint[0], nextPoint[0]));
            if (max >= threshold)
                break;
        }

    }

    public Image getImage() {
        return image;
    }

    public void summarize() {
        algorithm.summarize();
        System.out.printf("Max hits per pixel: %d\n", image.getMaxFrequency());
        System.out.printf("Max Velocity: %d\n", image.getMaxVelocity());
        System.out.printf("Total Points Generated: %d\n", image.getTotalPoints());
        System.out.printf("Unique points Generated: %d (%d%%)\n", image.getUniquePoints(),
                (int) (100 * image.getUniquePoints() / (size * size)));
    }
}
