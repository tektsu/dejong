package com.xecko.attr;

public class Scaler {
    private double scale;
    private double translate;

    public Scaler(double scale, double translate) {
        this.scale = scale;
        this.translate = translate;
    }

    public double scale(double v) {
        return v * scale + translate;
    }

    public double[] scalePoint(double[] point) {
        double[] p = new double[2];
        p[0] = scale(point[0]);
        p[1] = scale(point[1]);

        return p;
    }
}
