package com.xecko.attr;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Output {

    private String baseFilename;
    private String format;

    public Output(String directory, String file, String format) throws FileNotFoundException {
        directory = directory.replaceFirst("^~", System.getProperty("user.home"));
        File dir = new File(directory);
        if (dir.exists()) {
            if (!dir.isDirectory()) {
                throw new FileNotFoundException("Output directory exists as a file: " + directory);
            }
        } else {
            if (!dir.mkdir()) {
                throw new FileNotFoundException("Unable to create output directory: " + directory);
            }
        }
        baseFilename = directory + "/" + file;
        this.format = format;
    }

    public void writeImage(BufferedImage image) throws IOException {
        ImageIO.write(image, format, new File(baseFilename + "." + format));
    }

    public String getBaseFilename() {
        return baseFilename;
    }

    public void writeGray16(Image image) throws IOException {

        int size = image.getSize();
        Contrast contrast = new Contrast(image.getMaxFrequency());

        BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_USHORT_GRAY);
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                bi.setRGB(x, y, contrast.enhance(image.getFrequency(x, y)));
            }
        }

        writeImage(bi);
    }

    public void writeColor24(Image image, Colorize colorize, ColorScheme colorScheme) throws IOException {

        int max = colorScheme == ColorScheme.velocity ? image.getMaxVelocity() : image.getMaxFrequency();

        colorize.setLinearThresholds(max);
        colorize.showColors();

        int size = image.getSize();
        Contrast contrast = new Contrast(max);

        BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                if (colorScheme == ColorScheme.velocity) {
                    bi.setRGB(x, y, colorize.getColor(image.getFrequency(x, y), image.getMaxFrequency(),
                            image.getVelocity(x, y)));
                } else {
                    bi.setRGB(x, y, colorize.getColor(contrast.enhance(image.getFrequency(x, y))));
                }
            }
        }

        writeImage(bi);
    }
}
